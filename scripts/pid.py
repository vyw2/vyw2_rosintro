# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 14:56:37 2023

@author: Vincent
"""

import numpy as np

class PID:
    def __init__(self, Kp, Ki, Kd):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        
        # Error/Time Array
        self.prevValues = np.zeros((3, 3)) # Rows are error, time, derivative
        # Integral error
        self.intError = 0
    
    def reset(self):
        '''
        Function to reset all params in memory
        '''
        self.intError = 0
        self.prevValues.fill(0)
       
    def setKp(self, Kp):
        self.Kp = Kp
        
    def setKi(self, Ki):
        self.Ki = Ki
        
    def setKd(self, Kd):
        self.Kd = Kd
        
    def calcControlEffort(self, error, dt):
        # Update value array
        self.prevValues[0, 0], self.prevValues[0, 1], self.prevValues[0, 2] = error, self.prevValues[0, 0], self.prevValues[0, 1]
        self.prevValues[1, 0], self.prevValues[1, 1], self.prevValues[1, 2] = dt + self.prevValues[1, 0], self.prevValues[1, 0], self.prevValues[1, 1]
        
        # Update int.
        self.intError += error * dt

        self.prevValues[2, 0], self.prevValues[2, 1], self.prevValues[2, 2] = (self.prevValues[0, 1] - self.prevValues[0, 0]) / dt, self.prevValues[2, 0], self.prevValues[2, 1]
        
        # Add filtering of error here if needed to smooth out error and errorDerivative
        
        # Calculate control effort 
        ### TODO: Replace with filtered params ###
        controlOutput = self.Kp * self.prevValues[0, 0] + self.Ki * self.intError + self.Kd * self.prevValues[2, 0]

        return controlOutput
