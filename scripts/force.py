import sys
import rospy
import copy
import moveit_commander
import moveit_msgs.msg
import numpy as np
from geometry_msgs.msg import Pose
from geometry_msgs.msg import WrenchStamped
import geometry_msgs
import tf
import numpy as np

class PID:
    def __init__(self, Kp, Ki, Kd):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        
        # Error/Time Array
        self.prevValues = np.zeros((3, 3)) # Rows are error, time, derivative
        # Integral error
        self.intError = 0
    
    def reset(self):
        '''
        Function to reset all params in memory
        '''
        self.intError = 0
        self.prevValues.fill(0)
       
    def setKp(self, Kp):
        self.Kp = Kp
        
    def setKi(self, Ki):
        self.Ki = Ki
        
    def setKd(self, Kd):
        self.Kd = Kd
        
    def calcControlEffort(self, error, dt):
        # Update value array
        self.prevValues[0, 0], self.prevValues[0, 1], self.prevValues[0, 2] = error, self.prevValues[0, 0], self.prevValues[0, 1]
        self.prevValues[1, 0], self.prevValues[1, 1], self.prevValues[1, 2] = dt + self.prevValues[1, 0], self.prevValues[1, 0], self.prevValues[1, 1]
        
        # Update int.
        self.intError += error * dt

        self.prevValues[2, 0], self.prevValues[2, 1], self.prevValues[2, 2] = (self.prevValues[0, 1] - self.prevValues[0, 0]) / dt, self.prevValues[2, 0], self.prevValues[2, 1]
        
        # Add filtering of error here if needed to smooth out error and errorDerivative
        
        # Calculate control effort 
        ### TODO: Replace with filtered params ###
        controlOutput = self.Kp * self.prevValues[0, 0] + self.Ki * self.intError + self.Kd * self.prevValues[2, 0]

        return controlOutput

zForce = 0
def callback(msg):
    global zForce
    zForce = float(msg.wrench.force.z)

forceGoal = 100
controller = PID(0.1, 0.0, 0)
robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")
rospy.init_node('my_node')
rospy.Subscriber('/ft_sensor/raw', WrenchStamped, callback)
#get end-effector to point (x0,y0,z0)
def get_to_point(x,y,z):
    #define the position of the end-effector to go backwards from y-direction.
    pose_target = Pose()
    pose_target.position.x = x
    pose_target.position.y = y
    pose_target.position.z = z
    orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(0, 3.14, 0))
    pose_target.orientation = orientation
    group.set_pose_target(pose_target)
    #returns a boolean indicating whether the planning and execution was successful.
    success = group.go(wait=True)
    #ensures that there is no residual movement
    group.stop()
    #Clear targets after planning with poses.
    group.clear_pose_targets()

def cartisian_path(waypoints, wpose, dx, dy, dz, scale=1):
    wpose.position.x += scale * dx
    wpose.position.y += scale * dy  # First move downwards in (z)
    wpose.position.z += scale * dz
    waypoints.append(copy.deepcopy(wpose))
    return waypoints, wpose

#This function initializes the position of the robot arm to a point:(0.4,0.4,0.8).
def set_up(group):
    # initialize moveit_commander and a rospy node
    moveit_commander.roscpp_initialize(sys.argv)
    #Instantiate a MoveGroupCommander object. This is an interface to a planning group (group of joints).
    #group = moveit_commander.MoveGroupCommander("manipulator")
    #Set initial pose of the robot arm by assigning joint angles to avoid singularity.
    tau = 2 * np.pi
    joint_goal = group.get_current_joint_values()
    target = [-0.014810968624273002, -1.2788287341622357, 1.5131421932054856, -1.7962773110333075, -1.5708791009151026, 1.5550670045746555]
    joint_goal[0] = target[0]
    joint_goal[1] = target[1]
    # group.go(joint_goal, wait=True)
    joint_goal[2] = target[2]
    # group.go(joint_goal, wait=True)
    joint_goal[3] = target[3]
    joint_goal[4] = target[4]
    joint_goal[5] = target[5]  # 1/6 of a turn
    #reset ee position to start point (0.4,0.2,0.8).
    group.go(joint_goal, wait=True)
    group.stop()
    # get_to_point(0.4,0.2,0.9)
    joint_angles = group.get_current_joint_values()
    print("Start point Joint Angles:", joint_angles)

set_up(group)

b = -0.05/2
k = -0.05/1.0
y_values = np.arange(-0.5, 0.5, 0.01)

# z_values = 3*(20.2054426288154 * y_values**4 + 2.063284072492 * y_values**3 - 2.938295725343 * y_values**2 - 0.0964016320325 * y_values + 0.1439387341699)
z_values = k*y_values + b

input("Press Enter to Continue...")
for i in range(len(y_values)):
    wpose = group.get_current_pose().pose
    control = controller.calcControlEffort(zForce - forceGoal, 1)
    print("Val:", control)
    #zNext = wpose.position.z + control
    zNext = wpose.position.z
    get_to_point(0.4,y_values[i],zNext)


