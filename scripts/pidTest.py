# Import Statements
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
import numpy as np
from fpsim import surfaceSim
from pid import PID
import time
import matplotlib.pyplot as plt

# Define function to go to a preset initial pose
def resetPose():
	joint_goal = [0, -tau / 4, -tau / 6, -tau / 4, 0, 0]
	move_group.go(joint_goal, wait=True)
	move_group.stop()
	#initial_pose_goal = geometry_msgs.msg.Pose()
	#initial_pose_goal.orientation.w = 1.0
	#initial_pose_goal.position.x = -0.4
	#initial_pose_goal.position.y = -0.4
	#initial_pose_goal.position.z = 0.8
	#move_group.set_pose_target(initial_pose_goal)
	#success = move_group.go(wait=True)
	joint_goal = [0.980, -1.911, -0.62, -2.191, -1.571, 0.590]
	move_group.go(joint_goal, wait=True)
	move_group.stop()
	move_group.clear_pose_targets()

# Define function to go to initial starting point
def startPose(x, y):
	waypoints = []
	wpose = move_group.get_current_pose().pose
	wpose.position.y = y
	wpose.position.x = x
	waypoints.append(copy.deepcopy(wpose))
	(plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
	move_group.execute(plan, wait=True)
	move_group.stop()

# Initialize the main moveit planner, alongside the robot object and the movegroup to control joints
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
robot = moveit_commander.RobotCommander()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Reset pose
resetPose()

# Go to start position
startPose(0.8, 0.4)
input("Waiting...")
### CREATE FAKE FORCE SIMULATION -- REPLACE LATER
res = 0.01
stiffness = 10
forceGoal = 1
xRange = np.arange(0, 1.5, res)
yRange = (-3 * (xRange + 0.2 - 0.948) ** 6 + 2 * (xRange + 0.2 - 0.948) ** 5 - 4 * (xRange + 0.2 - 0.948) ** 3 + 2 * (xRange + 0.2 - 0.948) ** 2 + 2 * (xRange + 0.2 - 0.948) ** 1 + 1) / 6
desiredHeight = -1 * (forceGoal / stiffness) + yRange

simulator = surfaceSim(stiffness, yRange, res)
controller = PID(0.1, 0.02, 0)

# Go down to surface until target force is reached
force = 0
while force < forceGoal:
    waypoints = []
    wpose = move_group.get_current_pose().pose
    wpose.position.z -= 0.01
    waypoints.append(copy.deepcopy(wpose))
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    move_group.stop()
    force = simulator.getForce(0, wpose.position.z - 0.15)
    print(force, wpose.position.z)
    time.sleep(0.5)
    
# Drag across the -x direction while controlling for force
controlL = []
forceL = []
zL = []
for x in xRange:
    waypoints = []
    wpose = move_group.get_current_pose().pose
    force = simulator.getForce(x, wpose.position.z - 0.15)
    control = controller.calcControlEffort(force - forceGoal, 1)
    wpose.position.x -= res
    wpose.position.z += control
    print("Force: {}, Control: {}, Z: {}".format(force, control, wpose.position.z))
    controlL.append(control)
    forceL.append(force)
    zL.append(wpose.position.z)
    waypoints.append(copy.deepcopy(wpose))
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    move_group.stop()
    time.sleep(0.3)

plt.figure(1)
plt.plot(controlL)
plt.show()
plt.figure(2)
plt.plot(forceL)
plt.show()
plt.figure(3)
plt.plot(zL)
plt.show()
