#!/usr/bin/bash
rosservice call /reset

#Position turtles
rosservice call /spawn 8 2 1.5 turtle2
rosservice call /turtle1/teleport_absolute -- 1 8.5 -1.2

#Set pen styles and clear
rosservice call /turtle1/set_pen -- 255 125 0 5 0
rosservice call /turtle2/set_pen -- 125 255 255 50 0
rosservice call /clear

#Draw letters
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[6.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.4]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[6.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen -- 125 255 255 50 1
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rosservice call /turtle2/set_pen -- 125 255 255 50 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.2, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

#Move turtles out of the way
rosservice call /turtle1/set_pen -- 0 0 0 0 1
rosservice call /turtle1/teleport_absolute -- 0 0 0 
rosservice call /turtle2/set_pen -- 0 0 0 0 1
rosservice call /turtle2/teleport_absolute -- 0 0 0
