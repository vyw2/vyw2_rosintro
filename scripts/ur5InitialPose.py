# Import Statements
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initialize the main moveit planner, alongside the robot object and the movegroup to control joints
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
robot = moveit_commander.RobotCommander()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Set an initial set of joint values that uniquely define the robot orientation--essentially a "reset" to get the robot to a default position
joint_goal = [0, -tau / 4, -tau / 6, -tau / 4, 0, 0]
move_group.go(joint_goal, wait=True)
move_group.stop()

# Define the pose at which we should begin writing the initial and go to the starting pose
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = -0.4
pose_goal.position.y = 0.0
pose_goal.position.z = 0.8
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

# Set two pose goals for the rest of the initial and compute the path
pose_goal.position.x = -0.4
pose_goal.position.y = 0.2
pose_goal.position.z = 0.3
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

# Waypoint #3
pose_goal.position.x = -0.4
pose_goal.position.y = 0.4
pose_goal.position.z = 0.8
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
