# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 14:38:13 2023

@author: Vincent
"""

import numpy as np

class surfaceSim():
    
    # Give stiffness in N/cm and height per point in cm in the form of a 1-D numpy array, res = discrete point spacing in cm
    def __init__(self, stiffness, heightArray, res):
        self.stiffness = stiffness
        self.heightArray = heightArray
        self.res = res
        self.length = len(heightArray) * res
    
    def setheight(self, heightArray):
        self.heightArray = heightArray
        
    def setStiffness(self, stiffness):
        self.stiffness = stiffness
        
    def getForce(self, position, height):
        if position > self.length:
            print("Invalid Location")
            return None
        index = int(position / self.res)
        force = max(0, -1 * (height - self.heightArray[index]) * self.stiffness)
        return force
