# Import Statements
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Define function to go to a preset initial pose
def resetPose():
	joint_goal = [0, -tau / 4, -tau / 6, -tau / 4, 0, 0]
	move_group.go(joint_goal, wait=True)
	move_group.stop()
	#initial_pose_goal = geometry_msgs.msg.Pose()
	#initial_pose_goal.orientation.w = 1.0
	#initial_pose_goal.position.x = -0.4
	#initial_pose_goal.position.y = -0.4
	#initial_pose_goal.position.z = 0.8
	#move_group.set_pose_target(initial_pose_goal)
	#success = move_group.go(wait=True)
	joint_goal = [0.980, -1.911, -0.62, -2.191, -1.571, 0.590]
	move_group.go(joint_goal, wait=True)
	move_group.stop()
	move_group.clear_pose_targets()

# Define function to draw a "V" shape
def vDraw(vert = 0.5):
	waypoints = []
	wpose = move_group.get_current_pose().pose
	wpose.position.z -= vert
	wpose.position.y += 0.2
	waypoints.append(copy.deepcopy(wpose)) # Append the bottom of the V
	wpose.position.z += vert
	wpose.position.y += 0.2
	waypoints.append(copy.deepcopy(wpose)) # Append the top right of the V
	(plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
	move_group.execute(plan, wait=True)
	move_group.stop()

# Initialize the main moveit planner, alongside the robot object and the movegroup to control joints
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
robot = moveit_commander.RobotCommander()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Reset pose
resetPose()

# Draw First Initial
vDraw()

# Reset pose for next initial
resetPose()

# Draw second initial
vDraw(vert = 0.2)
waypoints = []
wpose = move_group.get_current_pose().pose
wpose.position.z -= 0.2
wpose.position.y -= 0.2
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= 0.3
waypoints.append(copy.deepcopy(wpose))
(plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0)
move_group.execute(plan, wait=True)
move_group.stop()

# Reset pose for final initial
resetPose()

# Draw last initial as 2 x First Initial
vDraw()
vDraw()
