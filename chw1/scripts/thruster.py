#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
import numpy as np

def callback(data):
    rospy.loginfo("Thrusters: Received: %s, Operating at Received Power", data.data)

def thruster():
    rospy.init_node('thruster')
    rospy.Subscriber('thruster', String, callback)
    rospy.spin()

if __name__ == '__main__':
    thruster()
