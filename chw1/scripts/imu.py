#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
import numpy as np

def imu():
	rospy.init_node('imu')
	imuData = rospy.Publisher('imuData', String, queue_size=10)
	rate = rospy.Rate(1)
	while not rospy.is_shutdown():
		message = str(np.random.rand(3))
		rospy.loginfo("Imu: Sending %s", message)
		imuData.publish(message)
		rate.sleep()

if __name__ == "__main__":
	try:
		imu()
	except rospy.ROSInterruptException:
		pass
