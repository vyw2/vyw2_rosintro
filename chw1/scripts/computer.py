#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
import numpy as np

imu = np.array([0.0, 0.0, 0.0])
camera = 0

def callbackCam(data):
	rospy.loginfo("Computer: Received: Camera Data: %s", data.data)
	global camera
	camera = float(data.data)
	message = str(imu + camera)
	rospy.loginfo("Computer: Sending: Thruster Data: %s", message)
	pub.publish(message)

def callbackIMU(data):
	rospy.loginfo("Computer: Received IMU Data: %s", data.data)
	global imu
	imu = np.array(eval(data.data.replace(",", "").replace("  ", ",").replace(" ", ",")))
	message = str(imu + camera)
	rospy.loginfo("Computer: Sending: Thruster Data: %s", message)
	pub.publish(message)

def computer():
	rospy.init_node('computer')
	rospy.Subscriber('imuData', String, callbackIMU)
	rospy.Subscriber('camData', String, callbackCam)
	global pub
	pub = rospy.Publisher('thruster', String, queue_size=10)
	rospy.spin()

	

if __name__ == '__main__':
	computer()
