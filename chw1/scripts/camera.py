#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
import numpy as np

def camera():
	rospy.init_node('camera')
	camData = rospy.Publisher('camData', String, queue_size=10)
	rate = rospy.Rate(1)
	while not rospy.is_shutdown():
		message = str(np.random.rand())
		rospy.loginfo("Camera Data: Sending: %s", message)
		camData.publish(message)
		rate.sleep()

if __name__ == "__main__":
	try:
		camera()
	except rospy.ROSInterruptException:
		pass
